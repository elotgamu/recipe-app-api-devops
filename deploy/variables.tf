variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-api-app-devops"
}

variable "contact" {
  default = "elotgamu@gmail.com"
}
